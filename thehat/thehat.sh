#!/bin/bash

showHat()
{
    hat=$( find -H "${PWD}" -maxdepth 1 -name '*.hat')

    clear
    for file in $hat; do
        cat $file
        sleep 0.5
        clear
    done
}

saymac()
{
    say -v whisper "$1" &
}

saylinux()
{
    espeak -v whisper "$1" &
}

say()
{
    if [ $(uname) = "Darwin" ]; then
        saymac "$1"
    elif [ $(uname) = "Linux" ]; then 
        saylinux "$1" 
    fi
}

draw()
{
    clear 

    echo "Starting the drawing goodluck!"
    echo "------------------"
    tput setaf 5;
    echo "Starting entries ${entries[*]}"
    tput sgr0;
    echo " "

    while [ "$nrOfEntries" -gt 0 ]; do
        echo " "
        read -p "hit enter to draw..."

        showHat 

        RAND_NUM=$((RANDOM % nrOfEntries))

        picked="${entries[$RAND_NUM]}" 

        new_array=()

        for value in "${entries[@]}"; do
            [[ $value != $picked ]] && new_array+=($value)
        done

        entries=("${new_array[@]}")
        unset new_array

        tput setaf 1; 
        echo "$picked was picked"
        tput sgr0;

        tput setaf 2;
        echo "Remaining entries: ${entries[*]}"
        tput sgr0;

        if [ $picked = "Casper" ]; then
            say "Kasper"
        else 
            say "$picked"
        fi

        nrOfEntries=$(($nrOfEntries-1))

        if [ $nrOfEntries = 0 ]; then
            read -p "continue..."
            break;
        fi
    done
}

custom()
{
    echo "Custom"

    read -p "How many entries do you want biiitch? " nrOfEntries

    for ((i=1; i <= $nrOfEntries;i++)); 
    do
        read -p "Give entry "$i": " ENTRY
        entries+=("$ENTRY")
    done

    draw
}

standard()
{
    echo "Standard"

    entries=("Martin" "Kaj" "Carsten" "Casper" "René" "Rune")
    nrOfEntries=6

    draw
}

modes=("standard" "custom")
options=("Pick Next")
entries=()
nrOfEntries=0

PS3="Choose a mode: "

select mode in "${modes[@]}"
do
    case $mode in 
        "standard") standard ;;
        "custom") custom ;;
        *) echo "usage shiat" ;;
    esac
done

exit
